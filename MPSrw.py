import discord, json, os
from discord.ext import commands
from discord.ext.commands import bot

if os.path.exists(os.getcwd() + "/config.json"):
    with open("./config.json")as f:
        configData = json.load(f)

else:
    configTemplate = {
    "Token": "<Bot Token Here>",
    "Prefix": "&",
    "GuildID": "<GuildID Here>"
    }
    with open(os.getcwd() + "/config.json", "w+") as f:
        json.dump(configTemplate, f)


token = configData["Token"]
prefix = configData["Prefix"]
guildID = int(configData["GuildID"])

bot = commands.Bot(command_prefix=[prefix])

for filename in os.listdir("./cogs"):
    if filename.endswith(".py"):
        bot.load_extension(f"cogs.{filename[:-3]}")

@bot.event
async def on_ready():
    print("Bot is ready!")

bot.run(token)